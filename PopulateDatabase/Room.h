#pragma once
#include"Item.h"
#include"RoomExits.h"
#include"RoomType.h"
#include"Enemy.h"


class Room
{
private:
	Entity m_entity;
	

public:
	RoomExits m_exits;
	std::string m_description{ "noRoomDescription" };
	RoomType m_roomType{ RoomType::Standard };
	std::vector<Enemy> m_enemies;
	std::vector<Item> m_items;

	//GETTERS SETTERS
	//can inline them if needet
	inline int getID()const { return m_entity.m_ID; };
	inline void setID(int id) { m_entity.m_ID = id; };
	inline std::string getEntName() const { return m_entity.m_name; };
	inline void setEntName(const std::string& toSet) { m_entity.m_name = toSet; };

	void printRoom() const;



};


namespace soci
{
	template<>
	struct type_conversion<Room>
	{
		typedef values base_type;

		static void from_base(values const& v, indicator /* ind */, Room& rm)
		{


			//itm.m_entity.m_ID = v.get<int>("entity_id");
			rm.setID(v.get<int>("entity_id"));
			//itm.m_entity.m_name = v.get<std::string>("entity_name" , "unknown");
			rm.setEntName(v.get<std::string>("entity_name", "unknown"));

			rm.m_exits.m_east = v.get<int>("east");
			rm.m_exits.m_north = v.get<int>("north");
			rm.m_exits.m_south = v.get<int>("south");
			rm.m_exits.m_west = v.get<int>("west");

			rm.m_description = v.get<std::string>("description");

			rm.m_roomType = static_cast<RoomType>(v.get<int>("room_type"));	
		}


		static void to_base(const Room& rm, values& v, indicator& ind)
		{
			v.set("entity_id", rm.getID());
			v.set("entity_name", rm.getEntName(), rm.getEntName().empty() ? i_null : i_ok);
			v.set("east", rm.m_exits.m_east);
			v.set("north", rm.m_exits.m_north);
			v.set("south", rm.m_exits.m_south);
			v.set("west", rm.m_exits.m_west);
			v.set("description", rm.m_description);
			int roomType = to_integral(rm.m_roomType);
			v.set("room_type", roomType);
			ind = i_ok;
		}

	};
}











