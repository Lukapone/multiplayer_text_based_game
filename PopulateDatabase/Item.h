#pragma once
#include <iostream>
#include <string>
#include <type_traits> //for std::underlying_type

#include"Entity.h"
#include"BaseStats.h"
#include"Price.h"
#include"SocialType.h"
#include"ItemType.h"
#include "soci.h"
//#include "soci-postgresql.h"

class Item
{
private:
	Entity m_entity;
public:
	BaseStats m_baseStats;
	Price m_price;
	SocialType m_socialBonus;
	ItemType m_itemType{ ItemType::Weapon };

	//CONSTRUCTORS
	Item() = default;
	Item(Entity entity, BaseStats baseStats, Price price, SocialType sType, ItemType itemType);

	//GETTERS SETTERS
	//can inline them if needet
	inline int getID()const { return m_entity.m_ID; };
	inline void setID(int id) { m_entity.m_ID = id; };
	inline std::string getEntName() const { return m_entity.m_name; };
	inline void setEntName(const std::string& toSet) { m_entity.m_name = toSet; };



	void printItem() const;


};

namespace soci
{
	template<>
	struct type_conversion<Item>
	{
		typedef values base_type;
		
		static void from_base(values const & v, indicator /* ind */, Item & itm)
		{
			

			//itm.m_entity.m_ID = v.get<int>("entity_id");
			itm.setID(v.get<int>("entity_id"));
			//itm.m_entity.m_name = v.get<std::string>("entity_name" , "unknown");
			itm.setEntName(v.get<std::string>("entity_name", "unknown"));

			itm.m_baseStats.m_AGI = v.get<int>("agi");
			itm.m_baseStats.m_STR = v.get<int>("str");
			itm.m_baseStats.m_BSTName = v.get<std::string>("base_name");

			itm.m_baseStats.m_Health.m_currentHealth = v.get<int>("curr_health");
			itm.m_baseStats.m_Health.m_maxHealth = v.get<int>("max_health");
			itm.m_baseStats.m_Health.m_regenAmount = v.get<int>("regen_amount");
		
			itm.m_itemType = static_cast<ItemType>(v.get<int>("item_type"));

			itm.m_socialBonus.m_socialPercentage = v.get<int>("social_percentage");
			itm.m_socialBonus.m_enemyType = static_cast<EnemyType>(v.get<int>("enemy_type"));

			//itm.firstName = v.get<std::string>("first_name");
			//itm.lastName = v.get<std::string>("last_name");

			//// p.gender will be set to the default value "unknown"
			//// when the column is null:
			//p.gender = v.get<std::string>("gender", "unknown");

			//// alternatively, the indicator can be tested directly:
			//// if (v.indicator("GENDER") == i_null)
			//// {
			////     p.gender = "unknown";
			//// }
			//// else
			//// {
			////     p.gender = v.get<std::string>("GENDER");
			//// }
		}

		
			static void to_base(const Item & itm, values & v, indicator & ind)
			{
				//v.set("entity_id", itm.m_entity.m_ID);
				//int id = itm.getID();
				v.set("entity_id", itm.getID());
				//v.set("entity_name", itm.m_entity.m_name, itm.m_entity.m_name.empty() ? i_null : i_ok);
				//std::string entName = itm.getEntName();
				v.set("entity_name", itm.getEntName(), itm.getEntName().empty() ? i_null : i_ok);
				v.set("agi", itm.m_baseStats.m_AGI);
				v.set("str", itm.m_baseStats.m_STR);
				v.set("base_name", itm.m_baseStats.m_BSTName);
				v.set("curr_health", itm.m_baseStats.m_Health.m_currentHealth);
				v.set("max_health", itm.m_baseStats.m_Health.m_maxHealth);
				v.set("regen_amount", itm.m_baseStats.m_Health.m_regenAmount);
				//auto value = to_integral(itm.m_itemType);
				int itmType = to_integral(itm.m_itemType);
				v.set("item_type", itmType);//can be replaced with v.set("item_type", to_integral(itm.m_itemType));
				v.set("social_percentage", itm.m_socialBonus.m_socialPercentage);
				int enemType = to_integral(itm.m_socialBonus.m_enemyType);
				v.set("enemy_type", enemType);
				ind = i_ok;		
			}
		
	};
}