#pragma once

struct Health
{
	int m_currentHealth{ 0 };
	int m_regenAmount{ 0 };
	int m_maxHealth{ 0 };
};