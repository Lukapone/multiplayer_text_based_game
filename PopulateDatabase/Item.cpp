#include "Item.h"

Item::Item(Entity entity, BaseStats baseStats, Price price, SocialType sType, ItemType itemType)
	:m_entity(entity), m_baseStats(baseStats), m_price(price), m_socialBonus(sType), m_itemType(itemType)
{
}

void Item::printItem() const
{
	m_entity.printEntity();
}
