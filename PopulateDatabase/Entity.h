#pragma once

#include<string>
#include<iostream>

struct Entity
{
	std::string m_name{ "emptyEntityName" };
	int m_ID{ 0 };

	void printEntity() const;
};