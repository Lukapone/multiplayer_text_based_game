#pragma once
//different types of rooms
enum class RoomType
{
	Standard,
	Common,
	Event,
	Shop
};
