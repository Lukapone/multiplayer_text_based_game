#include "soci.h"
#include "soci-postgresql.h"
#include <iostream>
#include <istream>
#include <ostream>
#include <string>
#include <exception>
#include "PersonTest.h"
#include "Item.h"
#include "Room.h"
#include "Player.h"
#include<vector>
//this is version for VS2015
using namespace soci;
using namespace std;

void createTablesForDB(session& sql);
void dropTablesFromDB(session& sql);
void populateItems(session & sql);
std::vector<Item> getItemsFromDatabaseTable(session &sql, int howMany);
void populateRooms(session& sql);
std::vector<Room> getRoomsFromDatabaseTable(session& sql);


int main()
{
	try
	{
		//can create new users remember dbname user and password required
		session sql(postgresql, "dbname=GameDB user=postgres password=Postgre2288");

		//sql << "CREATE TABLE rooms(entity_id int NOT NULL, entity_name varchar(20), east int, north int, south int, west int, description varchar(500), room_type int, PRIMARY KEY (entity_id))";


		////itm.m_entity.m_ID = v.get<int>("entity_id");
		//plr.setID(v.get<int>("entity_id"));
		////itm.m_entity.m_name = v.get<std::string>("entity_name" , "unknown");
		//plr.setEntName(v.get<std::string>("entity_name", "unknown"));

		//plr.setPassword(v.get<std::string>("plr_password"));

		//sql << "CREATE TABLE players(entity_id int NOT NULL, entity_name varchar(20), plr_password varchar(64) NOT NULL, PRIMARY KEY (entity_name))";

		/*Player p1;
		p1.setID(1);
		p1.setEntName("lukas");
		p1.setPassword("lukas");

		sql << "insert into players(entity_id, entity_name, plr_password) "
			"values(:entity_id, :entity_name, :plr_password)", use(p1);*/


		/*Player p1;
		p1.setID(2);
		p1.setEntName("sam");
		p1.setPassword("sam");

		sql << "insert into players(entity_id, entity_name, plr_password) "
			"values(:entity_id, :entity_name, :plr_password)", use(p1);*/


		//WE CAN CHECK BY SELECTING IF THE PLAYER EXISTS AND IF NOT WE CAN LET HIM USE THE NAME
		//Player p1;
		std::string name{ "lukas" };
		//sql << "select * from players where entity_name = :entity_name", into(p1), use(name);

		std::string pass{ "empty" };
		std::string plr_name{ "empty" };
		//sql << "select plr_password from players where entity_name = :entity_name", into(pass), use(name);
		sql << "select entity_name, plr_password from players where entity_name = :entity_name", into(plr_name), into(pass), use(name);
		//populateRooms(sql);

		//p1.printPlayer();

		std::cout << pass << std::endl;
		std::cout << plr_name << std::endl;



		////works with name and id
		//Room room;
		//std::string name{ "Kitchenn" };
		//int id{ 1 };
		////sql << "select * from rooms where entity_name = :entity_name", into(room), use(name);
		//sql << "select * from rooms where entity_id = :entity_id", into(room), use(id);
		//room.printRoom();







		/*std::vector<Room> loadedRooms = getRoomsFromDatabaseTable(sql);
		for (const auto& room : loadedRooms)
		{
			room.printRoom();
		}*/


		/*int count;
		sql << "select count(*) from items", into(count);
		std::cout << "Num of items in the database: " << count << std::endl;
		std::vector<Item> loadedItems = getItemsFromDatabaseTable(sql, count);
		for (const auto & item : loadedItems)
		{
			item.printItem();
		}*/


	}
	catch (soci::postgresql_soci_error const & e)
	{
		cerr << "PostgreSQL error: " << e.sqlstate()
			<< " " << e.what() << endl;
	}
	catch (exception const &e)
	{
		cerr << "Error: " << e.what() << '\n';
	}

	system("PAUSE");
}

void createTablesForDB(session & sql)
{
	//sql << "CREATE TABLE items(entity_id int NOT NULL, entity_name varchar(20),agi int, str int, base_name varchar(20), curr_health int, max_health int, regen_amount int, item_type int, social_percentage int, enemy_type int, PRIMARY KEY (entity_id))";
}

void dropTablesFromDB(session & sql)
{
	//sql << "DROP TABLE items";
}

void populateItems(session &sql)
{

	//session sql(postgresql, "dbname=GameDB user=postgres password=Postgre2288");
	/*Item item;
	item.m_entity.m_ID = 1;
	item.m_entity.m_name = "Sword";
	item.m_baseStats.m_AGI = 100;
	item.m_baseStats.m_STR = 20;
	item.m_baseStats.m_BSTName = "Bname";
	item.m_baseStats.m_Health.m_currentHealth = 100;
	item.m_baseStats.m_Health.m_maxHealth = 100;
	item.m_baseStats.m_Health.m_regenAmount = 100;

	sql << "insert into items(entity_id, entity_name, agi, str, base_name, curr_health, max_health, regen_amount) "
		"values(:entity_id, :entity_name, :agi, :str, :base_name, :curr_health, :max_health, :regen_amount)", use(item);*/


	Item item;
	//item.m_entity.m_ID = 1;
	item.setID(1);
	//item.m_entity.m_name = "Sword";
	item.setEntName("Sword");
	sql << "insert into items(entity_id, entity_name, agi, str, base_name, curr_health, max_health, regen_amount, item_type, social_percentage, enemy_type) "
		"values(:entity_id, :entity_name, :agi, :str, :base_name, :curr_health, :max_health, :regen_amount, :item_type, :social_percentage, :enemy_type)", use(item);

	item.setID(1);
	item.setEntName("Shield");
	sql << "insert into items(entity_id, entity_name, agi, str, base_name, curr_health, max_health, regen_amount, item_type, social_percentage, enemy_type) "
		"values(:entity_id, :entity_name, :agi, :str, :base_name, :curr_health, :max_health, :regen_amount, :item_type, :social_percentage, :enemy_type)", use(item);

	item.setID(1);
	item.setEntName("Arrow");
	sql << "insert into items(entity_id, entity_name, agi, str, base_name, curr_health, max_health, regen_amount, item_type, social_percentage, enemy_type) "
		"values(:entity_id, :entity_name, :agi, :str, :base_name, :curr_health, :max_health, :regen_amount, :item_type, :social_percentage, :enemy_type)", use(item);

	/*item.m_entity.m_ID = 3;
	item.m_entity.m_name = "Shield";
	sql << "insert into items(entity_id, entity_name) "
		"values(:entity_id, :entity_name)", use(item);*/
}

std::vector<Item> getItemsFromDatabaseTable(session &sql, int howMany)
{
	std::vector<Item>myLoadedItems;
	myLoadedItems.reserve(howMany);
	Item item;
	for (int i = 1; i <= howMany; i++)
	{
		sql << "select * from items where entity_id = :entity_id", into(item), use(i);
		myLoadedItems.emplace_back(item);
	}

	/*int id = 123;
	sql << "delete from companies where id = " << id;*/

	return myLoadedItems;
	/*Item i1;
	sql << "select * from persons", into(p1);
	assert(p1.id == 1);
	assert(p1.firstName + p.lastName == "PatSmith");
	assert(p1.gender == "unknown");

	p.firstName = "Patricia";
	sql << "update persons set first_name = :first_name "
	"where id = :id", use(p);*/


}

void populateRooms(session & sql)
{
	std::vector<Room> toInsert;

	Room r1;
	r1.setID(1);
	r1.setEntName("Hall");
	toInsert.emplace_back(r1);

	r1.setID(2);
	r1.setEntName("Kitchen");
	toInsert.emplace_back(r1);

	r1.setID(3);
	r1.setEntName("Bathroom");
	toInsert.emplace_back(r1);

	for (const auto& room : toInsert)
	{
		sql << "insert into rooms(entity_id, entity_name, east, north, south, west, description, room_type) "
			"values(:entity_id, :entity_name, :east, :north, :south, :west, :description, :room_type)", use(room);
	}

}

std::vector<Room> getRoomsFromDatabaseTable(session & sql)
{
	//neet to know how many is there to populate
	int count;
	sql << "select count(*) from items", into(count);

	std::vector<Room>myLoadedRooms;
	myLoadedRooms.reserve(count);
	Room room;
	for (int i = 1; i <= count; i++)
	{
		sql << "select * from rooms where entity_id = :entity_id", into(room), use(i);
		myLoadedRooms.emplace_back(room);
	}

	return myLoadedRooms;
}
