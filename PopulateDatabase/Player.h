#pragma once

#include"Item.h"

class Player
{
private:
	Entity m_entity;
	BaseStats m_baseStats;
	std::string m_password;

public:
	
	//GETTERS SETTERS
	//can inline them if needet
	inline int getID()const { return m_entity.m_ID; };
	inline void setID(int id) { m_entity.m_ID = id; };
	inline std::string getEntName() const { return m_entity.m_name; };
	inline void setEntName(const std::string& toSet) { m_entity.m_name = toSet; };

	inline std::string getPassword() const { return m_password; };
	inline void setPassword(const std::string& toSet) { m_password = toSet; };

	void printPlayer() const;
};




namespace soci
{
	template<>
	struct type_conversion<Player>
	{
		typedef values base_type;

		static void from_base(values const& v, indicator /* ind */, Player& plr)
		{


			//itm.m_entity.m_ID = v.get<int>("entity_id");
			plr.setID(v.get<int>("entity_id"));
			//itm.m_entity.m_name = v.get<std::string>("entity_name" , "unknown");
			plr.setEntName(v.get<std::string>("entity_name", "unknown"));

			plr.setPassword(v.get<std::string>("plr_password"));
			
		}


		static void to_base(const Player& plr, values& v, indicator& ind)
		{
			v.set("entity_id", plr.getID());
			v.set("entity_name", plr.getEntName(), plr.getEntName().empty() ? i_null : i_ok);
			v.set("plr_password", plr.getPassword());
			
			ind = i_ok;
		}

	};
}