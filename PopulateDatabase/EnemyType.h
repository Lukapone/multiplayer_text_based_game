#pragma once

//c++11 the new enum class you can assign the walues as you wish for example troll can start at 8 or 20 whatever you want
enum class EnemyType
{
	Troll=0,
	Witch,
	Dragon
};

