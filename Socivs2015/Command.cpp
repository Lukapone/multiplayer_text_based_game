#include "Command.h"

//a default constructor: X()
//a copy constructor : X(const X&)
//a copy assignment : operator=(const X&)
//a move constructor : X(X&&)
//a move assignment : operator=(X&&)
//a destructor : ~X()


//class X2 {
//	int i{ 666 };
//	string s{ "qqq" };
//	int j{ 0 };
//public:
//	X2() = default;         // all members are initialized to their defaults
//	X2(int ii) :i{ ii } {}        // s and j initialized to their defaults
//								  // ...
//};


Command::Command(std::string cmd, std::string sh, std::function<void()> fToExecute)
	: m_Command(cmd),m_Shortcut(sh),m_ExecuteCommand(fToExecute)
{

}


//Command::Command(std::string cmd, std::string sh, std::function<void(std::string)> fToExecute)
//{
//}

void Command::executeCommand()
{
	m_ExecuteCommand();
}

//void Command::executeCommand(std::string sentence)
//{
//	m_CommandwSentence(sentence);
//}

bool Command::isCommandInSentence(const std::string toParse)
{

	//std::string REGEX_GET = R"((GET)\s\/(.+)\s(HTTP.+))";

	std::string REGEX_GET = R"(\b()" + m_Command + R"(|)" + m_Shortcut + R"()\b)";

	//std::string REGEX_GET = R"(\b)" + m_Command  + R"(\b)";

	//std::string REGEX_GET = R"((\b(attack)\b))";

	std::cout << REGEX_GET << std::endl;
	std::cout << toParse << std::endl;

	//function to get the file that client want from http request using regex


	std::regex rx(REGEX_GET);
	std::smatch pieces_match;
	if (std::regex_search(toParse, pieces_match, rx))
	{
		/*for (auto x : pieces_match)
			std::cout << x << std::endl;*/
		return true;
	}

	return false;//not found in the sentence
}

void Command::ifFoundExecute(const std::string toParse)
{

	//std::string REGEX_GET = R"((GET)\s\/(.+)\s(HTTP.+))";

	std::string REGEX_GET = R"(\b()" + m_Command + R"(|)" + m_Shortcut + R"()\b)";

	//std::string REGEX_GET = R"(\b)" + m_Command  + R"(\b)";

	//std::string REGEX_GET = R"((\b(attack)\b))";

	std::cout << REGEX_GET << std::endl;
	std::cout << toParse << std::endl;

	//function to get the file that client want from http request using regex


	std::regex rx(REGEX_GET);
	std::smatch pieces_match;
	if (std::regex_search(toParse, pieces_match, rx))
	{
		//m_ExecuteCommand();
	}
}