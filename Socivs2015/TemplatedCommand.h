#pragma once

#include <string>
#include <functional>
#include <regex>
#include <iostream>

template<typename Callable>
//requred F callable function or functor or lambda
class TCommand
{
private:
	std::string m_Command{ "emptyCommand" };
	std::string m_Shortcut{ "emptyShortcut" };
	//Callable *m_Callable;
	Callable m_Callable;
	//std::function<void()> m_ExecuteCommand;
public:

	TCommand() = default;
	TCommand(std::string cmd, std::string sh, Callable c);


	template<class... Args>
	decltype(auto) executeCommand(Args&&... args)
	{
		return std::invoke(m_Callable, std::forward<Args>(args)...);
	}

	template<class... Args>
	decltype(auto) operator()(Args&&... args) {
		return std::invoke(m_Callable, std::forward<Args>(args)...);
	}

	template<class... Args>
	decltype(auto) executeBind(Args&&... args)
	{
		return m_Callable(std::forward<Args>(args)...);
	}
	/*void executeCommand()
	{
		return std::invoke(m_Callable);
	}*/

	bool isCommandInSentence(const std::string sentence);
	void ifFoundExecute(const std::string toParse);


};

template<typename Callable>
inline TCommand<Callable>::TCommand(std::string cmd, std::string sh, Callable c)
	:m_Command(cmd), m_Shortcut(sh), m_Callable(c)
{
}
