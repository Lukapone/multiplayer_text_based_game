#pragma once

#include<thread>
#include<iostream>
#include<mutex>
#include <atomic>         // std::atomic

#include"ConnectionWithThread.h"
#include"ConnectionManager.h"
#include"QuedSockets.h"

namespace Lukas {
	static constexpr int sc_MaxBuffered = 8192;// 8 kbytes
	static constexpr int sc_Sentimeout = 5;// 8 kbytes
	static constexpr int sc_Maxdatarate = 1024;// 8 kbytes
	//class with running thread processing clients one at the time
	template<typename protocol, typename defaulthandler>
	class RunnableThreadClass {
	private:
		std::thread the_thread;	
		//use atomic bool here
		std::atomic<bool> stop_thread;
		//bool stop_thread = false; // Super simple thread stopping. replaced with atomic
		void sendRecvInThread(QuedSockets & connection);

	public:
		/* Explicitly using the default constructor to
		* underline the fact that it does get called */
		RunnableThreadClass() : the_thread(), stop_thread(false) {}

		RunnableThreadClass(RunnableThreadClass&& other) 
			: stop_thread(other.stop_thread.load())
		{} //http://stackoverflow.com/questions/14182258/c11-write-move-constructor-with-atomicbool-member
		//move constructor required for threads since they are only moveable and std::atomic too

		//deleted in thread and atomic
		RunnableThreadClass(RunnableThreadClass const&) = delete;
		RunnableThreadClass& operator =(RunnableThreadClass const&) = delete;

		~RunnableThreadClass() {
			stop_thread = true;
			if (the_thread.joinable()) the_thread.join();
		}

		void Start(QuedSockets & dataSockets) {
			// This will start the thread. Notice move semantics!
			the_thread = std::thread(&RunnableThreadClass<protocol, defaulthandler>::sendRecvInThread, this, std::ref(dataSockets));
		}
	

	};

	template<typename protocol, typename defaulthandler>
	inline void RunnableThreadClass<protocol, defaulthandler>::sendRecvInThread(QuedSockets & dataSockets)
	{
		//this will automatically remove another datasocket
		while (!stop_thread)
		{
			if (dataSockets.Empty())
			{
				std::this_thread::sleep_for(std::chrono::seconds(5)); //just to not run on processor if no work to process
			}
			else
			{
				//atomic operation on WorkQue where i remove and create datasocket later used to construct ConnectionWithThread
				DataSocket removedToThread(dataSockets.remove());
				std::cout << "thread with id :" << GetCurrentThreadId() << "accepted datasocket "  << std::endl;
				// turn the socket into a connection
				ConnectionWithThread<protocol> conn(removedToThread);
				//we can decide to turn connection into blocking or nonblocking
				conn.SetBlocking(false);
				ConnectionWithThread<protocol>& cRef = conn;
				// give the connection its default handler
				

				cRef.AddHandler(new defaulthandler(cRef));
				//this will loop and receive and send data to client until i close the connection or client overload server with data
				while (conn.m_Closed == false)
				{
					try
					{
						conn.SendBuffer();
						conn.Receive();
							
						if (conn.GetCurrentDataRate() > sc_Maxdatarate ||
							conn.GetBufferedBytes() > sc_MaxBuffered ||
							conn.GetLastSendTime() > sc_Sentimeout)
						{
							std::cout << "problem" << std::endl;
							conn.Close();
							conn.Handler()->Hungup();
							conn.CloseSocket();

						}
					}
					catch (...)
					{
						conn.Close();//sets the boolean to true
						conn.Handler()->Hungup();
						conn.CloseSocket();//handlers closed here
					}		
				}
			}
		}
	}

}