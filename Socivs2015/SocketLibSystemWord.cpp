#include "SocketLibSystemWord.h"

namespace Lukas
{
	SystemWord::SystemWord()
	{
		int iResult = WSAStartup(MAKEWORD(2, 2), &m_WSAData);
		if (iResult != 0) {
			printf("WSAStartup failed: %d\n", iResult);
			WSACleanup();
			system("PAUSE");
		}
	}

	SystemWord::~SystemWord()
	{
		WSACleanup();
	}

}