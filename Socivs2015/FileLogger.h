#pragma once

// MUD Programming
// Ron Penton
// (C)2003
// BasicLibLogger.h - This contains the Logger class, which will log text
// to a file.


#ifndef BASICLIBLOGGER_H
#define BASICLIBLOGGER_H

// ========================================================================
//  Include Files
// ========================================================================
#include <string>
#include <fstream>
#include <iostream>
#include <chrono>
#include <ctime>

namespace BasicLib
{
	// ============================================================
	// This prints a timestamp in 24 hours hh:mm:ss format
	// ============================================================
	std::string TimeStamp();


	// ============================================================
	// This prints a datestamp in YYYY:MM:DD format
	// ============================================================
	std::string DateStamp();

	class TextDecorator
	{
	public:
		static std::string FileHeader(const std::string& p_title)
		{
			return "==================================================\n" +
				p_title + "\n" +
				"==================================================\n\n";
		}

		static std::string SessionOpen()
		{
			return "\n";
		}

		static std::string SessionClose()
		{
			return "\n";
		}

		static std::string Decorate(const std::string& p_string)
		{
			return p_string + "\n";
		}
	};


	template<class decorator>
	class Logger
	{
	public:
		Logger(const std::string& p_filename,
			const std::string& p_logtitle,
			bool p_timestamp = false,
			bool p_datestamp = false);

		~Logger();
		void Log(const std::string& p_entry);

	protected:
		std::fstream m_logfile;
		bool m_timestamp;
		bool m_datestamp;
	private:
		std::mutex m_MutexLog;

	};


	typedef Logger<TextDecorator> TextLog;



	template<class decorator>
	Logger<decorator>::Logger(const std::string& p_filename,
		const std::string& p_logtitle,
		bool p_timestamp,
		bool p_datestamp)
	{
		// now the tricky part... testing to see if a file is open or not.
		// stupid C++. You need to open a file in read mode, and if it doesn't
		// open correctly, you know that it doesn't exist.
		std::fstream filetester(p_filename.c_str(), std::ios::in);

		if (filetester.is_open())
		{
			// the file exists, so just close the test file
			filetester.close();

			// open the real file
			m_logfile.open(p_filename.c_str(), std::ios::out | std::ios::app);
		}
		else
		{
			// file doesn't exist.
			m_logfile.open(p_filename.c_str(), std::ios::out);

			// print out a file header to the file
			m_logfile << decorator::FileHeader(p_logtitle);
		}

		// print out an opening statement. Make sure it is time-and-date-stamped
		// as well.
		m_timestamp = true;
		m_datestamp = true;
		m_logfile << decorator::SessionOpen();
		Log("Session opened.");
		m_timestamp = p_timestamp;
		m_datestamp = p_datestamp;

	}

	template< class decorator >
	Logger< decorator >::~Logger()
	{
		m_timestamp = true;
		m_datestamp = true;
		Log("Session closed.");
		m_logfile << decorator::SessionClose();

	}


	template< class decorator >
	void Logger< decorator >::Log(const std::string& p_entry)
	{
		std::lock_guard<std::mutex> lg(m_MutexLog);

		std::cout << "this is adding date and timestamp to the logging the file" << std::endl;
		std::string message;

		if (m_datestamp)
		{
			message += "[" + TimeStamp() + "] ";
		}
		if (m_timestamp)
		{
			message += "[" + DateStamp() + "] ";
		}

		message += p_entry;
		m_logfile << decorator::Decorate(message);
		m_logfile.flush();
	}




	

	//std::string GetDateStamp() //http://stackoverflow.com/questions/12835577/how-to-convert-stdchronotime-point-to-calendar-datetime-string-with-fraction
	//{
	//	std::chrono::system_clock::time_point p = std::chrono::system_clock::now();

	//	std::time_t t = std::chrono::system_clock::to_time_t(p);
	//	//std::cout << std::ctime(&t) << std::endl; // for example : Tue Sep 27 14:21:13 2011
	//	return std::ctime(&t);
	//}


	// ============================================================
	// This prints a timestamp in 24 hours hh:mm:ss format
	// ============================================================
	std::string TimeStamp()
	{
		char str[9];

		// get the time, and convert it to struct tm format
		time_t a = time(0);
		struct tm b;
		localtime_s(&b, &a);

		// print the time to the string
		strftime(str, 9, "%H:%M:%S", &b);

		return str;
	}


	// ============================================================
	// This prints a datestamp in YYYY:MM:DD format
	// ============================================================
	std::string DateStamp()
	{
		char str[11];

		// get the time, and convert it to struct tm format
		time_t a = time(0);
		struct tm b;
		localtime_s(&b, &a);

		// print the time to the string
		strftime(str, 11, "%Y.%m.%d", &b);

		return str;
	}





} // end namespace BasicLib


#endif
