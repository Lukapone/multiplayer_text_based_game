#pragma once

#include "SocketLibIncludes_Types.h"
#include <stdio.h>
#include <string>
#include <iostream>


namespace Lukas
{

	class Socket
	{
	protected:
		sock m_Socket;
		struct sockaddr_in m_LocalInfo;
		bool m_IsBlocking;
		int m_Error;
		Socket( sock socket = -1);
	public:
		~Socket();

		void Close();
		void SetBlocking(bool blockMode);
		//getters
		inline ipaddress GetLockalAddress() const { return m_LocalInfo.sin_addr.s_addr; };
		inline sock GetSock() const { return m_Socket; };
		inline port GetLocalPort() const { return ntohs(m_LocalInfo.sin_port); };
	};

	class DataSocket :public Socket
	{
	protected:
		bool m_IsConnected;
		struct sockaddr_in m_RemoteInfo;

	public:
		DataSocket(sock socket = -1);

		void Connect(ipaddress p_addr, port p_port);
		int Send(const char* p_buffer, int p_size);
		int Receive(char* p_buffer, int p_size);
		void Close();

		inline ipaddress GetRemoteAddress() const { return m_RemoteInfo.sin_addr.s_addr; };
		inline port GetRemotePort() const { return ntohs(m_RemoteInfo.sin_port); };
		inline bool IsConnected() const { return m_IsConnected; };


	};





	class ListeningSocket : public Socket
	{
	protected:
		bool m_IsListening;


	public:
		ListeningSocket();

		void Listen(port p_port, std::string ip_Address);
		DataSocket Accept();
		void Close();


		inline bool IsListening() const { return m_IsListening; };


	};










}















