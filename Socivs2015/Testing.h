#pragma once
//a default constructor: X()
//a copy constructor : X(const X&)
//a copy assignment : operator=(const X&)
//a move constructor : X(X&&)
//a move assignment : operator=(X&&)
//a destructor : ~X()
#include<string>
#include<iostream>
using entityid = unsigned int;
using l_string = std::string;

std::string LowerCase(const std::string& p_string)
	{
		std::string str = p_string;

		for (size_t i = 0; i < str.size(); i++)
		{
			str[i] = tolower(str[i]);
		}
		return str;
	}

class Entity
{
private:
	l_string m_name{"UNDEFINED"};
	entityid m_id{0};

	


public:
	/*Entity(l_string&& name, entityid&& id)
		: m_name(name), m_id(id)
	{
		std::cout << "move ctor" << std::endl;
	}*/

	/*Entity(l_string name, entityid id)
		: m_name{ std::move(name) }, m_id{ id }
	{
		std::cout << "copy ctor" << std::endl;
	}*/

	/*Entity(const l_string& name,const entityid& id)
		: m_name{ std::move(name) }, m_id{ id }
	{
		std::cout << "ref ctor" << std::endl;
	}*/

	template<typename T,typename I>
	Entity(T&& name, I&& id)
		: m_name{ std::forward<T>(name) }, m_id{ std::forward<I>(id) }
	{
		//std::cout << "lvalueref ctor" << std::endl;
	}

	
	
	inline l_string CompName() const
	{
		return LowerCase(m_name);
	}

	inline bool MatchFull(const l_string& p_str) const
	{
		return CompName() == LowerCase(p_str);
	}


};


struct matchentityfull
{
	matchentityfull(const l_string& p_str)
		: m_str(p_str) { /* do nothing */
	}

	bool operator() (const Entity& p_entity)
	{
		return p_entity.MatchFull(m_str);
	}

	bool operator() (Entity* p_entity)
	{
		return p_entity != 0 && p_entity->MatchFull(m_str);
	}


	l_string m_str;
};


// --------------------------------------------------------------------
//  This performs a double-pass search on a collection of iterators,
//  which, in addition, must also qualify using a qualifier functor
// --------------------------------------------------------------------
template<class iterator, class firstpass, class secondpass, class qualify>
inline iterator double_find_if(iterator begin,
	iterator end,
	firstpass one,
	secondpass two,
	qualify q)
{
	iterator itr = begin;
	while (itr != end)
	{
		if (q(*itr) && one(*itr))
			return itr;
		++itr;
	}

	itr = begin;
	while (itr != end)
	{
		if (q(*itr) && two(*itr))
			return itr;
		++itr;
	}

	return itr;
}