#pragma once

#include <vector>
#include <algorithm>
#include <random>

#include "Enemy.h"
#include"HelperFunctions.h"


//std::random_device m_Rd; https://msdn.microsoft.com/en-us/library/bb982398.aspx
//std::uniform_int_distribution<int> dist();
//std::mt19937 m_Mt;


//could be std::array if know size at advance //http://stackoverflow.com/questions/4424579/stdvector-versus-stdarray-in-c


class EnemyHandler
{
private:

	std::vector<Enemy> m_PossibleEnemies;
public:

	void add(Enemy& toAdd);
	std::vector<Enemy> generateRoomEnemys(int number);
	std::vector<Enemy> generateRoomEnemysSeedet(int number, int seed);
	void shuffleVector();

};

