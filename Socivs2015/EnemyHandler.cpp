#include "EnemyHandler.h"



void EnemyHandler::add(Enemy & toAdd)
{
	m_PossibleEnemies.push_back(toAdd);
}

std::vector<Enemy> EnemyHandler::generateRoomEnemys(int number)
{
	std::vector<Enemy> toRetEnem;
	int generatedNum{ 0 };
	//generate int numbers between 0 and m_PossibleEnemies.size() and push them into vector
	for (int i = 0;i < number;i++)
	{
		generatedNum = getRandomMt19937(0, m_PossibleEnemies.size()-1);
		toRetEnem.push_back(m_PossibleEnemies[generatedNum]);
	}

	return toRetEnem;
}

std::vector<Enemy> EnemyHandler::generateRoomEnemysSeedet(int number, int seed)
{
	std::vector<Enemy> toRetEnem;
	int generatedNum{ 0 };
	//generate int numbers between 0 and m_PossibleEnemies.size() and push them into vector

	for (int i = 0;i < number;i++)
	{
		generatedNum = getRandomIntWithSeed(seed + i,0, m_PossibleEnemies.size() - 1);
		toRetEnem.push_back(m_PossibleEnemies[generatedNum]);
	}

	return toRetEnem;
}

void EnemyHandler::shuffleVector()
{
	std::random_device rd;
	std::mt19937 mt(rd());
	std::shuffle(m_PossibleEnemies.begin(), m_PossibleEnemies.end(), mt);
}
