#pragma once


#include <memory>
#include <string>

#include"Level.h"
#include "Inventory.h"

//#include "Room.h"
#include"Enemy.h"

class Player
{
private:

	Level m_Level;
	Inventory m_Inventory;

	int m_Money;
	int m_SocialSkill;
	std::string m_Rank;
	//std::unique_ptr<Room> m_Room_ptr;

public:
	
	void fight(std::string sentence);
	
	void fightEnemy(Enemy& toFight);


};

