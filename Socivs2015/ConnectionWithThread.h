#pragma once

#include "SocketLibLukasIncludes.h"
#include <string>
#include <chrono>
#include<thread>
#include <stack>
#include <time.h>
#include <stdio.h>

#include"includeGSL\gsl.h"

namespace Lukas
{

	const static int BUFFERSIZE = 1024;
	const static int TIMECHUNK = 16;

template< class protocol >
class ConnectionWithThread : public DataSocket
{


private:

	


public:

	using Ms = std::chrono::milliseconds;
	using Sec = std::chrono::seconds; //defined as long long if used with auto
	using sys_clock = std::chrono::system_clock;// represent wall clock time from the system-wide realtime clock, can be cast to timepoint
	using sd_clock = std::chrono::steady_clock;//That is, the clock may not be adjusted
	using hr_clock = std::chrono::high_resolution_clock; //represent clocks with the shortest tick period.(since start pc )
	using a_clock = hr_clock;//can be swapped easy for any you need
	using timePoint = std::chrono::time_point<a_clock>;
	using sint64 = long long;

	protocol m_Protocol; //default is telnet
	std::string m_SendBuffer;
	//canot use time_point because of computations
	sint64 m_CreationTime;
	sint64 m_LastSendTime;
	sint64 m_LastReceiveTime;

	int m_Datarate;
	int m_LastDataRate;
	bool m_CheckSendTime;
	char m_Buffer[BUFFERSIZE];
	bool m_Closed;

	//std::thread m_ConnectionThread;

	std::stack< typename protocol::handler* > m_Handlerstack;





	~ConnectionWithThread();
	ConnectionWithThread();
	ConnectionWithThread(DataSocket& p_socket);
	ConnectionWithThread(ConnectionWithThread<protocol>&& p_socket);



	void Initialize();

	// ------------------------------------------------------------------------
	// This puts data into the sending queue.
	// ------------------------------------------------------------------------
	void BufferData(gsl::array_view<const char> buffer);

	void SendBuffer();

	void Receive();

	inline protocol& Protocol() { return m_Protocol; }

	inline bool Closed() { return m_Closed; }

	inline void Close() { m_Closed = true; }

	inline void CloseSocket() 
	{ 
		DataSocket::Close();
		ClearHandlers();
	}

	// ------------------------------------------------------------------------
	// This gets the current receiving datarate for the socket, in bytes per
	// second.
	// ------------------------------------------------------------------------
	inline int GetCurrentDataRate() const
	{
		return m_Datarate / TIMECHUNK;
	}

	inline int GetBufferedBytes() const
	{
		return (int)m_SendBuffer.size();
	}

	sint64 GetLastSendTime()const
	{
		if (m_CheckSendTime)
		{
			return std::chrono::duration_cast<Sec>(a_clock::now().time_since_epoch()).count() - m_LastSendTime;
		}
		return 0;
	}

	void printTimestampFromSystemClock(const timePoint& toPrint);



	//All handler stuff
	inline void SwitchHandler(typename protocol::handler* p_handler)
	{
		if (Handler())
		{
			Handler()->Leave();     // leave the current state if it exists
			delete Handler();       // delete state
			m_Handlerstack.pop();   // pop the pointer off
		}

		m_Handlerstack.push(p_handler);
		p_handler->Enter();     // enter the new state
	}

	inline void AddHandler(typename protocol::handler* p_handler)
	{
		if (Handler())
			Handler()->Leave(); // leave the current state if it exists
		m_Handlerstack.push(p_handler);
		p_handler->Enter();     // enter the new state
	}

	inline void RemoveHandler()
	{
		Handler()->Leave();     // leave current state
		delete Handler();       // delete state
		m_Handlerstack.pop();   // pop the pointer off
		if (Handler())         // if old state exists, 
			Handler()->Enter(); // tell it connection has re-entered
	}


	inline typename protocol::handler* Handler()
	{
		if (m_Handlerstack.size() == 0)
			return 0;
		return m_Handlerstack.top();
	}


	void ClearHandlers()
	{
		// leave the current handler
		if (Handler())
			Handler()->Leave();

		// delete all the handlers on the stack
		while (Handler())
		{
			delete Handler();
			m_Handlerstack.pop();
		}
	}









};


template<class protocol>
inline ConnectionWithThread<protocol>::~ConnectionWithThread()
{
	//stop_thread = true;

	//if (m_ConnectionThread.joinable()) m_ConnectionThread.join();
	//m_ConnectionThread.join();
}

 //------------------------------------------------------------------------
 //Constructors. One constructs a normal connection, and the other accepts
 //a DataSocket to use as a "template" for the connection
 //------------------------------------------------------------------------
template< class protocol >
ConnectionWithThread<protocol>::ConnectionWithThread()
{
	Initialize();
}

template< class protocol >
ConnectionWithThread<protocol>::ConnectionWithThread(DataSocket& p_socket)
	: DataSocket(p_socket)
{
	std::cout << "Default" << std::endl;
	Initialize();
}

template<class protocol>
inline ConnectionWithThread<protocol>::ConnectionWithThread(ConnectionWithThread<protocol>&& other)
	:m_Closed(std::forward<bool>(other.m_Closed)),
	m_ConnectionThread(std::forward<std::thread>(other.m_ConnectionThread)), 
	m_CreationTime(std::forward<timePoint>(other.m_CreationTime)),
	m_Handlerstack(std::forward<std::stack< typename protocol::handler* >>(other.m_Handlerstack)), 
	m_Protocol(std::forward<protocol>(other.m_Protocol)),
	m_SendBuffer(std::forward<std::string>(other.m_SendBuffer))
{

	std::cout << "moved" << std::endl;
}





template< class protocol >
void ConnectionWithThread<protocol>::Initialize()
{
	m_Datarate = 0;
	m_LastDataRate = 0;
	//can initialize with zero or clock time
	m_LastReceiveTime = 0;
	m_LastSendTime = std::chrono::duration_cast<Sec>(a_clock::now().time_since_epoch()).count();
	m_CheckSendTime = false;
	m_CreationTime = std::chrono::duration_cast<Sec>(a_clock::now().time_since_epoch()).count();
	m_Closed = false;
}



// ------------------------------------------------------------------------
// This puts data into the sending queue.
// ------------------------------------------------------------------------
template< class protocol >
void ConnectionWithThread<protocol>::BufferData(gsl::array_view<const char> buffer)
{
	m_SendBuffer.append(buffer.data(), buffer.length());
}

template<class protocol>
inline void ConnectionWithThread<protocol>::SendBuffer()
{
	if (!m_SendBuffer.empty())
	{
		int sent = Send(m_SendBuffer.data(), (int)m_SendBuffer.size());
		std::cout << "Send() sent : " << sent << "data" << std::endl;
		m_SendBuffer.erase(0, sent);

		if (sent > 0)
		{
			// since data was sent successfully, reset the send time.
			m_LastSendTime = std::chrono::duration_cast<Sec>(a_clock::now().time_since_epoch()).count();
			//this is the time from when the chosen clock begin ticking epoch_time
			//std::cout << "Send() marked down Seconds HRC : " << m_LastSendTime << '\n';

			m_CheckSendTime = false;
		}
		else
		{
			// no data was sent, so mark down that the sending time
			// needs to be checked, to see if the socket is entering
			// client deadlock
			if (!m_CheckSendTime)
			{
				// if execution gets here, that means that this connection
				// has previously not had any problems sending data, so
				// mark down the time that the sending problem started.
				// note that it may have started earlier, but since we didn't
				// start sending data earlier, there really is no way to know
				// for sure when it started.
				m_CheckSendTime = true;
				m_LastSendTime = std::chrono::duration_cast<Sec>(a_clock::now().time_since_epoch()).count();
				//std::cout << "Send() marked down time the sending problem started : " << std::endl;
			}
		}   // end no-data-sent check

	} // end buffersize check
}

template<class protocol>
inline void ConnectionWithThread<protocol>::Receive()
{

	//std::cout << "inside of recv" << std::endl;
	int bytes = DataSocket::Receive(m_Buffer, BUFFERSIZE);
	// get the current time
	sint64 currentTime = std::chrono::duration_cast<Sec>(a_clock::now().time_since_epoch()).count();
	//std::cout << "Recv() marked down time for datarate : "<< currentTime << std::endl;

	if ((m_LastReceiveTime / TIMECHUNK) != (currentTime / TIMECHUNK))
	{
		m_LastDataRate = m_Datarate / TIMECHUNK;
		//std::cout << "Last Datarate : " << m_LastDataRate << std::endl;
		m_Datarate = 0;
		m_LastReceiveTime = currentTime;
		
	}

	m_Datarate += bytes;
	//std::cout << " Datarate+bytes : " << m_LastDataRate << std::endl;

	// tell the protocol policy object about the received data.
	m_Protocol.Translate(*this, m_Buffer, bytes);
}



template<class protocol>
inline void ConnectionWithThread<protocol>::printTimestampFromSystemClock(const timePoint & toPrint)
{
	time_t now_c = std::chrono::system_clock::to_time_t(toPrint);
	char printBuffer[26];
	ctime_s(printBuffer, sizeof printBuffer, &now_c);
	printf("%s", printBuffer);
}











}