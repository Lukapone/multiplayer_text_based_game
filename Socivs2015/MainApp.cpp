
#include "SocketLibLukasIncludes.h"
#include <iostream>

#include <thread>
#include <mutex>
#include <condition_variable>

#include "includeGSL\gsl.h"

#include"Command.h"
#include"Parser.h"

#include"ConnectionHandler.h"
#include"ConnectionManager.h"
#include"ConnectionWithThread.h"
#include"ListenningManager.h"
#include"GameDatabase.h"
#include"LukasChat.h"
#include"LukasLogon.h"
#include"Telnet.h"

#include"RunnableThreadClass.h"


int main()
{
	using namespace Lukas;


#define MULTITHREADED 1
#if MULTITHREADED

	SystemWord WSAS;
	ListeningManager<Telnet, SCLogon> lm;
	ConnectionManager<Telnet, SCLogon> cm;
	cm.startWorkThreads();

	lm.SetConnectionManager(&cm);
	lm.StartOnPort5099();

	for (int i = 0;i < 10;i++)
	{
		lm.Listen();
	}

#endif

	system("PAUSE");

	return 0;
}