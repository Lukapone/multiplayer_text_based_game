#pragma once

#include<vector>

#include"ConnectionWithThread.h"
#include"RunnableThreadClass.h"
#include"QuedSockets.h"

namespace Lukas
{

	// Forward Declarations
	template<typename protocol>
	class ConnectionWithThread;



// ============================================================================
// Description: This connection manager class will manage connections, 
// all identified with an ID. 
// ============================================================================
template<typename protocol, typename defaulthandler>
class ConnectionManager
{

	

	using conVector = std::vector< ConnectionWithThread<protocol> >;
private:
	// a list of all the connections within the manager
	conVector m_Connections;
	//std::vector<DataSocket> m_WorkQue;
	QuedSockets m_WorkSockets;
	//std::vector<std::thread> m_ConnectionThreads;
	std::vector<RunnableThreadClass<protocol, defaulthandler>> m_MyRunningThreads;
public:

	



	~ConnectionManager();
	// ------------------------------------------------------------------------
	//  This notifies the manager that there is a new connection available
	// ------------------------------------------------------------------------
	void NewConnection(DataSocket& p_socket);
	
	void CheckDeathConnections();

	

	void Recv();
	void Send();
	void sendRecvInThread(DataSocket connection);

	void startWorkThreads();
	void createThread();
	void startThread();
};





template<typename protocol, typename defaulthandler>
inline ConnectionManager<protocol, defaulthandler>::~ConnectionManager()
{
	////m_ConnectionThread.join();
	//for (auto& conThread : m_ConnectionThreads)
	//{
	//	conThread.join();
	//}
}


#define VECTORSERVER 1
#if VECTORSERVER

template<typename protocol, typename defaulthandler>
inline void ConnectionManager<protocol, defaulthandler>::startWorkThreads()
{
	std::cout << "STARTING WORK THREADS" << std::endl;
	int threadsInt{ 1000 }; //for testing my machine limit is 1000 threads
	//create threads
	for (int i = 0; i < threadsInt; i++)
	{
		RunnableThreadClass<protocol, defaulthandler> oneThreadInClass;
		m_MyRunningThreads.push_back(std::move(oneThreadInClass));	
	}

	//start them with move semantics
	for (int i = 0; i < threadsInt; i++)
	{
		//std::cout << "thread " << i << "started" << std::endl;
		m_MyRunningThreads[i].Start(m_WorkSockets);
	}

}

template<typename protocol, typename defaulthandler>
inline void ConnectionManager<protocol, defaulthandler>::NewConnection(DataSocket & p_socket)
{
	//add the socket into work que
	m_WorkSockets.add(p_socket);
}



#endif

template<typename protocol, typename defaulthandler>
void ConnectionManager<protocol, defaulthandler>::Recv()
{

	for (auto& con : m_Connections)
	{
		con.Receive();
	}

}

template<typename protocol, typename defaulthandler>
inline void ConnectionManager<protocol, defaulthandler>::Send()
{

	for (auto& conn : m_Connections)
	{
		conn.SendBuffer();
	}


}

}