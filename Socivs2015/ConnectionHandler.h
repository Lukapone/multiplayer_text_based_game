#pragma once


#ifndef CONNECTIONHANDLER_H
#define CONNECTIONHANDLER_H

#include "SocketLibIncludes_Types.h"
#include "ConnectionWithThread.h"


namespace Lukas
{

	template<typename protocol, typename command>
	class ConnectionHandler
	{

	public:
		//The using syntax has an advantage when used within templates.
		//	If you need the type abstraction, but also need to keep template 
		//	parameter to be possible to be specified in future.You should write 
		//	something like this http://stackoverflow.com/questions/10747810/what-is-the-difference-between-typedef-and-using-in-c11


		using conn =  ConnectionWithThread<protocol>;

		// ------------------------------------------------------------------------
		//  Constructor, records pointer to the connection.
		// ------------------------------------------------------------------------
		ConnectionHandler(conn& p_conn) : m_connection(&p_conn) {}
		virtual ~ConnectionHandler() {};

		// ------------------------------------------------------------------------
		//  This handles incomming commands. Anything passed into this function
		//  is assumed to be a complete command from a client.
		// ------------------------------------------------------------------------
		virtual void Handle(command p_data) = 0;

		// ------------------------------------------------------------------------
		//  This notifies the handler that a connection is entering the state
		// ------------------------------------------------------------------------
		virtual void Enter() = 0;

		// ------------------------------------------------------------------------
		//  This notifies the handler that a connection is leaving the state.
		// ------------------------------------------------------------------------
		virtual void Leave() = 0;

		// ------------------------------------------------------------------------
		//  This notifies the handler that a connection has unexpectedly hung up.
		// ------------------------------------------------------------------------
		virtual void Hungup() = 0;

		// ------------------------------------------------------------------------
		//  This notifies the handler that a connection is being kicked due to 
		//  flooding the server.
		// ------------------------------------------------------------------------
		virtual void Flooded() = 0;

	protected:
		conn* m_connection;//could be unique pointer if needet

	};


}   // end namespace SocketLib
#endif
