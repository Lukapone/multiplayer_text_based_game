#pragma once


struct Level
{
	int m_ExperienceBar;
	int m_CurLevel;
	int m_MaxLevel;
	int m_NextLevelExp;
};