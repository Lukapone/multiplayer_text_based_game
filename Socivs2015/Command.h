#pragma once

#include <string>
#include <functional>
#include <regex>
#include <iostream>

class Command
{
private:
	std::string m_Command{ "emptyCommand" };
	std::string m_Shortcut{ "emptyShortcut" };
	std::function<void()> m_ExecuteCommand;
public:

	Command() = default;
	Command(std::string cmd,std::string sh,std::function<void()> fToExecute);
	void executeCommand();
	bool isCommandInSentence(const std::string sentence);
	void ifFoundExecute(const std::string toParse);


};

