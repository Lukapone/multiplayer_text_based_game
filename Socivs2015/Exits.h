#pragma once


struct Exits
{
	//we can use std::bind to bind string to Id or just enum class
	int m_North;
	int m_South;
	int m_East;
	int m_West;

};