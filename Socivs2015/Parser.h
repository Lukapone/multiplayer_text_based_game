#pragma once

#include<vector>
#include<string>
#include<regex>
#include"Command.h"

class Parser
{
private:
	
	std::vector<Command> m_AllCommands;

public:
	
	bool parse(const std::string& toParse);
	void parseAndExecute(const std::string & toParse);

	void addComand(const Command& toAdd);

};


